#include "MenuUtil.h"

void MenuUtil::ShowMenu()
{
	std::cout << "<--------------- Menu --------------->" << std::endl;
	std::cout << "1.Afisare grafic pe procente ale utilizatorilor dupa sex" << std::endl;
	std::cout << "2.Varsta medie a utilizatorilor care au mai mult de 100 de prieteni" << std::endl;
	std::cout << "3.Adaugare automata a prietenilor in csv / afisare (pe baza id-ului)" << std::endl;
	std::cout << "4.Cel mai varstnic prieten din lista de prieteni al unui utilizator selectat" << std::endl;
	std::cout << "5.Grupare utilizatori dupa nume" << std::endl;
	std::cout << "6.Afisare numar utilizatori cu acelasi nume" << std::endl;
	std::cout << "7.Media numarului de prieteni ai utilizatorilor" << std::endl;
	std::cout << "8.Iesire" << std::endl;
	std::cout << "Scrie numarul optiunii dorite: "<< std::endl;
}
