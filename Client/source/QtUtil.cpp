#include "QtUtil.h"

void QtUtil::DrawChart_GroupByAgeGender(const std::vector<double> men, const std::vector<double> women, const QColor& menBarColor, const QColor& womenBarColor)
{
	double maleProcent = men.at(0);
	double femaleProcent = women.at(0);

#pragma region  Init Qt window

	char *argv[] = { "Test Qt", "arg1", "arg2", NULL };
	int argc = sizeof(argv) / sizeof(char*) - 1;
	QApplication app(argc, argv);

#pragma endregion

	std::string set0Text = std::to_string(maleProcent) + "% MALE";
	std::string set1Text = std::to_string(femaleProcent) + "% FEMALE";

	/* Assign names to the set of bars used */
	QBarSet *set0 = new QBarSet(set0Text.c_str());
	QBarSet *set1 = new QBarSet(set1Text.c_str());

	/* Set bars color - default blue and pink */
	set0->setBrush(menBarColor);
	set1->setBrush(womenBarColor);

	/* Assign values for each bar */
	for (int i = 1; i < men.size(); ++i) // We will start from 1 cause the 0 pos. is the overall procent
	{
		*set0 << men[i];
		*set1 << women[i];
	}

	/* Add all sets of data to the chart as a whole */
	QBarSeries *series = new QBarSeries();

	series->append(set0);
	series->append(set1);

	/* Used to define the bar chart to display title, legend */
	QChart *chart = new QChart();

	/* Add the chart */
	chart->addSeries(series);

	// Set title
	chart->setTitle("USERS AGE INTERVAL BY GENDER");

	/* Define starting animation */
	chart->setAnimationOptions(QChart::AllAnimations); // NoAnimation, GridAxisAnimations, SeriesAnimations

	/* Holds the category titles */
	QStringList categories;
	categories << "0-18 age" << "19-30 age" << "31-100 age";

	/* Adds categories to the axes */
	QBarCategoryAxis *axis = new QBarCategoryAxis();
	axis->append(categories);
	chart->createDefaultAxes();

	/* 1. Bar chart */
	chart->setAxisX(axis, series);

	/* 2. Stacked Bar chart */
	// chart->setAxisY(axis, series);

	/* Define where the legend is displayed */
	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);

	/* Used to display the chart */
	QChartView *chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	/* Used to change the palette */
	QPalette pal = qApp->palette();

	/* Change the color around the chart widget and text */
	pal.setColor(QPalette::Window, QRgb(0xffffff));
	pal.setColor(QPalette::WindowText, QRgb(0x404044));

	/* Apply palette changes to the application */
	qApp->setPalette(pal);

	/* Create the main app window */
	QMainWindow window;

	/* Set the main window widget */
	window.setCentralWidget(chartView);
	window.resize(420, 300);
	window.show();

	/* Execute the app */
	app.exec();
}

void QtUtil::DrawPieChart_GroupByAgeGender(const std::vector<double> men, const std::vector<double> women)
{

#pragma region  Init Qt window

	char *argv[] = { "Test Qt", "arg1", "arg2", NULL };
	int argc = sizeof(argv) / sizeof(char*) - 1;
	QApplication app(argc, argv);

#pragma endregion

	QPieSeries *series = new QPieSeries();
	series->setLabelsPosition(QPieSlice::LabelInsideHorizontal);

#pragma region Define Some Colors
	QBrush blue = QColor::fromRgb(133, 169, 226);
	QBrush blue2 = QColor::fromRgb(58, 103, 175);
	QBrush blue3 = QColor::fromRgb(57, 52, 193);

	QBrush pink = QColor::fromRgb(249, 129, 225);
	QBrush pink2 = QColor::fromRgb(221, 37, 83);
	QBrush pink3 = QColor::fromRgb(217, 66, 237);
#pragma endregion

	/* Assign values for each slice */
	for (int i = 1; i < men.size(); ++i)
	{
		series->append("", men[i]);
	}

	for (int i = 1; i < women.size(); ++i)
	{
		series->append("", women[i]);
	}

	// Add label to slices
	QPieSlice *slice0 = series->slices().at(0);
	slice0->setLabelVisible();
	slice0->setBrush(blue);

	QPieSlice *slice1 = series->slices().at(1);
	slice1->setLabelVisible();
	slice1->setBrush(blue2);

	QPieSlice *slice2 = series->slices().at(2);
	slice2->setLabelVisible();
	slice2->setBrush(blue3);

	QPieSlice *slice3 = series->slices().at(3);
	slice3->setLabelVisible();
	slice3->setBrush(pink);

	QPieSlice *slice4 = series->slices().at(4);
	slice4->setLabelVisible();
	slice4->setBrush(pink2);

	QPieSlice *slice5 = series->slices().at(5);
	slice5->setExploded();
	slice5->setPen(QPen(Qt::darkMagenta, 2));
	slice5->setLabelVisible();
	slice5->setBrush(pink3);

	/* Create the chart widget */
	QChart *chart = new QChart();

	/* Show the procent of each slice */
	for (auto slice : series->slices())
		slice->setLabel(QString("%1%").arg(100 * slice->percentage(), 0, 'f', 1));

	/* Add data to chart with title and hide legend */
	chart->addSeries(series);
	chart->setTitle("Users by their age and gender");
	chart->legend()->show();

	/* Used to display the chart */
	QChartView *chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	/* Create the main app window */
	QMainWindow window;

	/* Set the main window widget */
	window.setCentralWidget(chartView);
	window.resize(420, 300);
	window.show();

	app.exec();
}

void QtUtil::DrawChart_GroupByName(std::vector<std::string> users)
{
#pragma region  Init Qt window

	char *argv[] = { "Test Qt", "arg1", "arg2", NULL };
	int argc = sizeof(argv) / sizeof(char*) - 1;
	QApplication app(argc, argv);

#pragma endregion

	QBarSeries *series = new QBarSeries();
	/* Assign values for each bar */
	int name = 0;
	int nameNumber = 1;
	for (int i = 0; nameNumber < users.size(); ++i) // We will start from 1 cause the 0 pos. is the overall procent
	{
		QBarSet *setBar = new QBarSet(users.at(name).c_str());
		*setBar << std::atoi(users.at(nameNumber).c_str());
		nameNumber += 2;
		name += 2;

		/* Add all sets of data to the chart as a whole */
		series->append(setBar);

	}

	/* Used to define the bar chart to display title, legend */
	QChart *chart = new QChart();

	/* Add the chart */
	chart->addSeries(series);

	// Set title
	chart->setTitle("GROUP BY NAME");

	/* Define starting animation */
	chart->setAnimationOptions(QChart::AllAnimations); // NoAnimation, GridAxisAnimations, SeriesAnimations

	/* Holds the category titles */
	QStringList categories;
	categories << "Names";

	/* Adds categories to the axes */
	QBarCategoryAxis *axis = new QBarCategoryAxis();
	axis->append(categories);
	chart->createDefaultAxes();

	/* 1. Bar chart */
	chart->setAxisX(axis, series);

	/* 2. Stacked Bar chart */
	// chart->setAxisY(axis, series);

	/* Define where the legend is displayed */
	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);

	/* Used to display the chart */
	QChartView *chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	/* Used to change the palette */
	QPalette pal = qApp->palette();

	/* Change the color around the chart widget and text */
	pal.setColor(QPalette::Window, QRgb(0xffffff));
	pal.setColor(QPalette::WindowText, QRgb(0x404044));

	/* Apply palette changes to the application */
	qApp->setPalette(pal);

	/* Create the main app window */
	QMainWindow window;

	/* Set the main window widget */
	window.setCentralWidget(chartView);
	window.resize(420, 300);
	window.show();

	/* Execute the app */
	app.exec();
}
