#include "SessionUtil.h"

SessionUtil::SessionUtil(const boost::beast::multi_buffer& buffer) :
	buffer(buffer)
{
}

std::string SessionUtil::GetProperty(const std::string& property)
{
	boost::property_tree::ptree result;
	std::stringstream ss;

	ss << boost::beast::buffers(this->buffer.data());

	boost::property_tree::read_json(ss, result);
	return result.get<std::string>(property);
}

void SessionUtil::WriteResponseToFile()
{
	std::string file = this->GetProperty("File");
	boost::filesystem::ofstream ofs(file);
	ofs << boost::beast::buffers(this->buffer.data());
}
