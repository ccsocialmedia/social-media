#pragma once

#include "UsersOldestFriendRequest.h"

UsersOldestFriendRequest::UsersOldestFriendRequest() : Request("UserOldestFriend")
{
	std::string id;
	
	std::cout << "Select the id of user that you want to know the oldest friend" << std::endl;
	std::cin >> id;

	/* This will be send to the server */
	this->content.push_back(boost::property_tree::ptree::value_type("User_ID", id));
}
