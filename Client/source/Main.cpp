#include <iostream>
#include <memory>

#include "Session.h"
#include "MenuUtil.h"

int main(int argc, char** argv)
{
	//TestQt();

	const auto host = "127.0.0.1";
	const auto port = "1313";
	
	int option = 0;
	while (option != 8) {

		// The io_context is required for all I/O
		boost::asio::io_context ioContext;
		RequestsManager requestsManager;

		system("pause");
		system("cls");

		MenuUtil::ShowMenu();
		std::cin >> option;
		switch (option) {
		case 1: 
		{
			auto groupAgeGender = requestsManager.getMap().at("GroupByAgeGender")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, groupAgeGender);
			ioContext.run();
			break;
		}
		case 2: 
		{
			auto usersAgeByFriends = requestsManager.getMap().at("UsersAgeByFriends")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, usersAgeByFriends);
			ioContext.run();
			break;
		}
		case 3:
		{
			auto userFriends = requestsManager.getMap().at("UserFriends")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, userFriends);
			ioContext.run();
			break;
		}
		case 4:
		{
			requestsManager.RequestsManagerForOldestFriend();
			auto userOldestFriend = requestsManager.getMap().at("UserOldestFriend")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, userOldestFriend);
			ioContext.run();
			break;
		}
		case 5:
		{
			auto groupByName = requestsManager.getMap().at("GroupByName")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, groupByName);
			ioContext.run();
			break;
		}
		case 6:
		{
			requestsManager.RequestsManagerForSameName();
			auto nrOfSameName = requestsManager.getMap().at("NumberWithSameName")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, nrOfSameName);
			ioContext.run();
			break;
		}
		case 7:
		{
			auto averageFriends = requestsManager.getMap().at("AverageFriends")->getContentAsString();
			std::make_shared<Session>(ioContext)->run(host, port, averageFriends);
			ioContext.run();
			break;
		}
		case 8:
		{
			system("pause");
			return EXIT_SUCCESS;
		}
		}
	}	

}

