#pragma once

#include <memory>
#include "QtUtil.h"
#include <iostream>
#include <string>
#include <vector>

#pragma region BOOST_INCLUDES
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/foreach.hpp>
#pragma endregion

#include "Session.h"

Session::Session(boost::asio::io_context& ioContext) : resolver(ioContext), webSocket(ioContext)
{
	/* EMPTY */
}


// Start the asynchronous operation
void Session::Session::run(char const* host, char const* port, const std::string& message)
{
	// Save these for later
	this->host = host;
	this->message = message;

	// Look up the domain name
	this->resolver.async_resolve(
		this->host,
		port,
		std::bind(
			&Session::on_resolve,
			shared_from_this(),
			std::placeholders::_1,
			std::placeholders::_2));
}

void Session::on_resolve(boost::beast::error_code errorCode, boost::asio::ip::tcp::resolver::results_type results)
{
	if (errorCode)
	{
		std::cerr << "resolve" << ": " << errorCode.message() << "\n";
	}

	// Make the connection on the IP address we get from a lookup
	boost::asio::async_connect(
		this->webSocket.next_layer(),
		results.begin(),
		results.end(),
		std::bind(
			&Session::on_connect,
			shared_from_this(),
			std::placeholders::_1));
}

void Session::on_connect(boost::beast::error_code errorCode)
{
	if (errorCode)
	{
		std::cerr << "connect" << ": " << errorCode.message() << "\n";
	}

	// Perform the boost::boost::beast::websocket handshake
	this->webSocket.async_handshake(this->host, "/",
		std::bind(
			&Session::on_userFriends,
			shared_from_this(),
			std::placeholders::_1));
}

void Session::on_userFriends(boost::beast::error_code errorCode)
{
	if (errorCode)
	{
		std::cerr << "user friends" << ": " << errorCode.message() << "\n";
	}

	auto requestAsBuffer = boost::asio::buffer(this->message);

	// Send the message
	this->webSocket.async_write(
		requestAsBuffer,
		std::bind(
			&Session::on_write,
			shared_from_this(),
			std::placeholders::_1,
			std::placeholders::_2));
}

void Session::on_write(boost::beast::error_code errorCode, std::size_t bytes_transferred)
{
	boost::ignore_unused(bytes_transferred);

	if (errorCode)
	{
		std::cerr << "write" << ": " << errorCode.message() << "\n";
	}

	// Read a message into our buffer
	this->webSocket.async_read(
		this->buffer,
		std::bind(
			&Session::on_read,
			shared_from_this(),
			std::placeholders::_1,
			std::placeholders::_2));
}

void Session::on_read(boost::beast::error_code errorCode, std::size_t bytes_transferred)
{
	boost::ignore_unused(bytes_transferred);

	if (errorCode)
	{
		std::cerr << "read" << ": " << errorCode.message() << "\n";
	}

	SessionUtil sessionUtil(this->buffer);
	std::string action = sessionUtil.GetProperty("Action");

	if (action == "GroupByAgeGender")
	{
		this->write_groupByAgeGender();
	}
	else if (action == "GroupByName")
	{
		this->write_groupByName();
	}
	else if (action == "UserFriends")
	{
		this->write_userFriends();
	}
	else if (action == "UsersAgeByFriends")
	{
		this->write_usersAgeByFriends();
	}
	else if (action == "UserOldestFriend")
	{
		this->write_userOldesFriend();
	}
	else if (action == "NumberWithSameName")
	{
		this->write_numberSameName();
	}
	else if (action == "AverageFriends")
	{
		this->write_averageFriends();
	}
	else //Default 
	{
		this->write_response_to_file();
	}

	/* Close the boost::boost::beast::websocket connection */
	this->webSocket.async_close(boost::beast::websocket::close_code::normal,
		std::bind(
			&Session::on_close,
			shared_from_this(),
			std::placeholders::_1));
}

void Session::write_response_to_file()
{
	SessionUtil sessionUtil(this->buffer);
	sessionUtil.WriteResponseToFile();
}

void Session::write_userFriends()
{
	SessionUtil sessionUtil(this->buffer);
	int usersNumber = std::atoi(sessionUtil.GetProperty("UsersNumber").c_str()) - 1;
	int i = 0;

	while (i != usersNumber)
	{
		std::string user = std::to_string(i);
		std::string friends = sessionUtil.GetProperty(user);
		++i;

		std::cout << "User " << user << " friends: " << friends << std::endl;
	}

	sessionUtil.WriteResponseToFile();
}
void Session::write_groupByAgeGender()
{
	SessionUtil sessionUtil(this->buffer);

	std::string maleFemaleProportion = sessionUtil.GetProperty("all procents");
	double maleProcent = std::stod(maleFemaleProportion.substr(0, 5));
	double femaleProcent = std::stod(maleFemaleProportion.substr(7, 5));

	double male0_18Procent = std::stod(sessionUtil.GetProperty("0-18m"));
	double male19_30Procent = std::stod(sessionUtil.GetProperty("19-30m"));
	double male31PlusProcent = std::stod(sessionUtil.GetProperty("31-100m"));

	double female0_18Procent = std::stod(sessionUtil.GetProperty("0-18f"));
	double female19_30Procent = std::stod(sessionUtil.GetProperty("19-30f"));
	double female31PlusProcent = std::stod(sessionUtil.GetProperty("31-100f"));

	std::cout << "Task: \"Varsta medie a utilizatorilor care au mai mult de 100 de prieteni" << std::endl;
	std::cout << "Procent of male users is: " << maleProcent << "% and female users procent is: " << femaleProcent << "%" << std::endl;
	std::cout << "Procent on 0-18 years old is: " << male0_18Procent << "% male and " << female0_18Procent << "% female" << std::endl;
	std::cout << "Procent on 19-30 years old is: " << male19_30Procent << "% male and " << female19_30Procent << "% female" << std::endl;
	std::cout << "Procent on 30-100 years old is: " << male31PlusProcent << "% male and " << female31PlusProcent << "% female" << std::endl;
	std::cout << "Attention!! In file, procentage is written with 2 decimals!!" << std::endl;

	sessionUtil.WriteResponseToFile();

	std::vector<double> men;
	men.push_back(maleProcent);
	men.push_back(male0_18Procent);
	men.push_back(male19_30Procent);
	men.push_back(male31PlusProcent);

	std::vector<double> women;
	women.push_back(femaleProcent);
	women.push_back(female0_18Procent);
	women.push_back(female19_30Procent);
	women.push_back(female31PlusProcent);

	this->draw_groupByAgeGender(men, women);
}

void Session::write_userOldesFriend()
{
	SessionUtil sessionUtil(this->buffer);
	int userID = std::atoi(sessionUtil.GetProperty("OldestFriendID").c_str());
	int userAge = std::atoi(sessionUtil.GetProperty("Age").c_str());

	std::cout << "Oldest friend id: " << userID << " ( AGE - " << userAge << " )";

	sessionUtil.WriteResponseToFile();
}

void Session::write_groupByName()
{
	SessionUtil sessionUtil(this->buffer);
	std::string usersName = sessionUtil.GetProperty("UsersName");
	typedef std::vector<std::string> Tokens;
	Tokens tokens;
	
	std::vector<std::string> users; // express for the QT

	boost::split(tokens, usersName, boost::is_any_of(" "));
	BOOST_FOREACH(const std::string& i, tokens) {
		if (i != " ") {
			std::string nameAparition = sessionUtil.GetProperty(i);
			int numberOfName = std::stoi(nameAparition);
			std::cout << "Numele " << i << " apare de " << numberOfName << " ori"<<std::endl;

			users.push_back(i);
			users.push_back(std::to_string(numberOfName));
		}
	}

	sessionUtil.WriteResponseToFile();
	this->draw_groupByName(users);
}

void Session::draw_groupByName(std::vector<std::string> users)
{
	QtUtil::DrawChart_GroupByName(users);
}

void Session::draw_groupByAgeGender(const std::vector<double>& men, const std::vector<double>& women)
{
	QtUtil::DrawChart_GroupByAgeGender(men, women);
	//QtUtil::DrawPieChart_GroupByAgeGender(men, women);
}

void Session::write_numberSameName()
{
	SessionUtil sessionUtil(this->buffer);
	std::string numberWithSameName = sessionUtil.GetProperty("Number of users");
	std::cout << "Numarul de useri cu acelasi nume este de: " << std::stoi(numberWithSameName) << std::endl;
	sessionUtil.WriteResponseToFile();
}
void Session::write_averageFriends()
{
	SessionUtil sessionUtil(this->buffer);
	std::string averageNumberFriends = sessionUtil.GetProperty("Average number of friends");
	std::cout << "Media numarului de prieteni " << std::stoi(averageNumberFriends) << std::endl;
	sessionUtil.WriteResponseToFile();
}
void Session::write_usersAgeByFriends()
{
	SessionUtil sessionUtil(this->buffer);
	std::string average = sessionUtil.GetProperty("UsersAverageFriendsAge");
	double usersAvgAgeFriends = std::stod(average);
	std::cout << "Average of users friends that has more than 100 friends is: " << usersAvgAgeFriends << std::endl;
}
void Session::on_close(boost::beast::error_code errorCode)
{
	// If we get here then the connection is closed gracefully
	if (errorCode)
	{
		std::cerr << "close" << ": " << errorCode.message() << "\n";
	}
}
