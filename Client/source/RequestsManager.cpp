#include "RequestsManager.h"
#include "GroupByAgeGenderRequest.h"
#include "UserFriendsRequest.h"
#include "UsersAgeByFriendsRequest.h"
#include "UsersOldestFriendRequest.h"
#include "GroupByNameRequest.h"
#include "UsersWithSameNameRequest.h"
#include "UsersAverageFriendsRequest.h"

RequestsManager::RequestsManager()
{
	this->requests.emplace("UserFriends", std::make_shared<UserFriendsRequest>());
	this->requests.emplace("GroupByAgeGender", std::make_shared<GroupByAgeGenderRequest>());
	this->requests.emplace("UsersAgeByFriends", std::make_shared<UsersAgeByFriendsRequest>());
	this->requests.emplace("GroupByName", std::make_shared<GroupByNameRequest>());
	this->requests.emplace("AverageFriends", std::make_shared<UsersAverageFriendsRequest>());
}

void RequestsManager::RequestsManagerForOldestFriend()
{
	this->requests.emplace("UserOldestFriend", std::make_shared<UsersOldestFriendRequest>());
}

void RequestsManager::RequestsManagerForSameName()
{
	this->requests.emplace("NumberWithSameName", std::make_shared<UsersWithSameNameRequest>());
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
