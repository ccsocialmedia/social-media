#pragma once

#include "UsersWithSameNameRequest.h"

UsersWithSameNameRequest :: UsersWithSameNameRequest() : Request("NumberWithSameName")
{
	std::string name;

	std::cout << "Introduce the name for the search: " << std::endl;
	std::cin >> name;

	/* This will be send to the server */
	this->content.push_back(boost::property_tree::ptree::value_type("Name", name));
}