#pragma once

#pragma region BOOST_INCLUDES
#include <boost/beast/websocket.hpp>
#include <boost/asio/strand.hpp>
#pragma endregion

#include "RequestsManager.h"
#include "SessionUtil.h"

class Session : public std::enable_shared_from_this<Session>
{
public:
	/* Constructors */
	explicit Session(boost::asio::io_context& ioc);

public:
	void run(char const* host, char const* port, const std::string& message);

	void on_resolve(boost::beast::error_code errorCode, boost::asio::ip::tcp::resolver::results_type results);
	void on_connect(boost::beast::error_code errorCode);
	void on_userFriends(boost::beast::error_code errorCode);

	void on_write(boost::beast::error_code errorCode, std::size_t bytes_transferred);
	void on_read(boost::beast::error_code errorCode, std::size_t bytes_transferred);

	/* Custom functions to print every task that we have until now */
	#pragma region Custom Print Functions
	void write_userFriends();
	void write_usersAgeByFriends();
	void write_groupByAgeGender();
	void write_userOldesFriend();
	void write_groupByName();
	void write_averageFriends();
	void write_numberSameName();
	#pragma endregion

	/* This function is will draw o chart for the task "Group by age gender" */
	void draw_groupByAgeGender(const std::vector<double>& men, const std::vector<double>& women);
	void draw_groupByName(std::vector<std::string> users);

	/* A function that write in a file every session given */
	void write_response_to_file();
	void on_close(boost::beast::error_code errorCode);

private:
	boost::asio::ip::tcp::resolver resolver;
	boost::beast::websocket::stream<boost::asio::ip::tcp::socket> webSocket;
	boost::beast::multi_buffer buffer;
	std::string host;
	std::string message;
};
