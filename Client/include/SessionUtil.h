#pragma once

#pragma region BOOST_INCLUDES
#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/asio/buffer.hpp>
#pragma endregion

#include <memory>

class SessionUtil
{
public:
	/* Constructors */
	SessionUtil() = default;
	SessionUtil(const boost::beast::multi_buffer& buffer);

	/* Destructors */
	~SessionUtil() = default;
public:
	std::string GetProperty(const std::string& property);
	void WriteResponseToFile();

private:
	boost::beast::multi_buffer buffer;
};