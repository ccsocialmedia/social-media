#pragma once

#pragma region QT_INCLUDES
#include <QApplication>
#include <QtWidgets/qlabel.h>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <QtCharts/QHorizontalStackedBarSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QCategoryAxis>
#include <QtCharts/QPieSeries>
#include <QtCharts/QPieSlice>
#include <boost/beast/websocket.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/foreach.hpp>
using namespace QtCharts;
//QT_CHARTS_USE_NAMESPACE
#pragma endregion

#include<vector>

#include "SessionUtil.h"

class QtUtil
{
public:
	/// DrawChart_GroupByAgeGender - this function draw a QT chart ///
	// parm men <=> is a vector of interval of men age by procentage
	// parm women <=> is a vector of interval of women age by procentage 
	// optional param menBarColor <=> is the color of bar chart that represent men, default is a white blue 
	// optional param womenBarColor <=> is the color of bar char that represent women, default is white pink
	static void DrawChart_GroupByAgeGender(const std::vector<double> men, const std::vector<double> women, 
		const QColor& menBarColor = QColor::fromRgb(135, 193, 255), const QColor& womenBarColor = QColor::fromRgb(253, 135, 255));

	static void DrawPieChart_GroupByAgeGender(const std::vector<double> men, const std::vector<double> women);
	static void DrawChart_GroupByName(std::vector<std::string> users);
};