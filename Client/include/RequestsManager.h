#pragma once

#include "Response.h"
#include "IDataPacketsManager.h"

class RequestsManager : public Framework::IDataPacketsManager<Framework::Request>
{
public:
	/* Constructors */
	RequestsManager();

	/* Destructors */
	~RequestsManager() = default;

public:
	void RequestsManagerForOldestFriend();
	void RequestsManagerForSameName();
	virtual std::map<std::string, std::shared_ptr<Framework::Request>> getMap() const override;
private:
	std::map<std::string, std::shared_ptr<Framework::Request>> requests;
};