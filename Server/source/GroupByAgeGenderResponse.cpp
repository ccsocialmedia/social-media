#include "GroupByAgeGenderResponse.h"
#include "Users.h"
#include <math.h>
#include <iomanip>
#include <sstream>
GroupByAgeGenderResponse::GroupByAgeGenderResponse() : Response("GroupByAgeGender")
{
	/* EMPTY */
}
std::string GroupByAgeGenderResponse::roundMethod(double var)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(2) << var;
	return stream.str();
}
std::string GroupByAgeGenderResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	/* Read the csv with users */
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();
	int usersNumber = users.GetUsers().size();

	
	int numberOf0_18AgeMaleUsers = 0;
	int numberOf19_30AgeMaleUsers = 0;
	int numberOf30PlusAgeMaleUsers = 0;

	int numberOf0_18AgeFemaleUsers = 0;
	int numberOf19_30AgeFemaleUsers = 0;
	int numberOf30PlusAgeFemaleUsers = 0;

	for each (User user in users.GetUsers()) 
	{
		int age = user.GetAge();
		Gender gender = user.GetGender();

		if (age >= 0 && age <= 18) 
		{
			if (gender == Gender::Male)
				numberOf0_18AgeMaleUsers++;
			else
				numberOf0_18AgeFemaleUsers++;
		}
		else if (age >= 19 && age <= 30) 
		{
			if (gender == Gender::Male)
				numberOf19_30AgeMaleUsers++;
			else
				numberOf19_30AgeFemaleUsers++;
		}
		else {
			if (gender == Gender::Male)
				numberOf30PlusAgeMaleUsers++;
			else
				numberOf30PlusAgeFemaleUsers++;
		}		
	}
	int numberOfAllMales = numberOf0_18AgeMaleUsers + numberOf19_30AgeMaleUsers + numberOf30PlusAgeMaleUsers;
	int numberOfAllFemales = numberOf0_18AgeFemaleUsers + numberOf19_30AgeFemaleUsers + numberOf30PlusAgeFemaleUsers;

	double varMale = ((double)(numberOfAllMales * 100) / usersNumber);
	double varFemale =  ((double)(numberOfAllFemales * 100) / usersNumber);

	std::string maleProcent = roundMethod(varMale);
	std::string femaleProcent = roundMethod(varFemale);
	
	std::string pushFormat = maleProcent + 'm' + '-' + femaleProcent + 'f';
	this->content.push_back(boost::property_tree::ptree::value_type("File", "AllProcentsGender.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("all procents",pushFormat));

	std::string male0_18Procent = roundMethod((double)(numberOf0_18AgeMaleUsers * 100) / usersNumber);
	this->content.push_back(boost::property_tree::ptree::value_type("0-18m", male0_18Procent));
	std::string male19_30Procent = roundMethod((double)(numberOf19_30AgeMaleUsers * 100) / usersNumber);
	this->content.push_back(boost::property_tree::ptree::value_type("19-30m", male19_30Procent));
	std::string male31PlusProcent = roundMethod((double)(numberOf30PlusAgeMaleUsers * 100) / usersNumber);
	this->content.push_back(boost::property_tree::ptree::value_type("31-100m", male31PlusProcent));
	
	std::string female0_18Procent = roundMethod((double)(numberOf0_18AgeFemaleUsers * 100) / usersNumber);
	this->content.push_back(boost::property_tree::ptree::value_type("0-18f", female0_18Procent));
	std::string female19_30Procent = roundMethod((double)(numberOf19_30AgeFemaleUsers * 100) / usersNumber);
	this->content.push_back(boost::property_tree::ptree::value_type("19-30f",female19_30Procent));
	std::string female31PlusProcent = roundMethod((double)(numberOf30PlusAgeFemaleUsers * 100) / usersNumber);
	this->content.push_back(boost::property_tree::ptree::value_type("31-100f", female31PlusProcent));

	return this->getContentAsString();	
}
