 #include "UsersAgeByFriendsResponse.h"
#include "Users.h"
#include <iomanip>
#include <sstream>
UsersAgeByFriendsResponse::UsersAgeByFriendsResponse() : Response("UsersAgeByFriends")
{
	/* EMPTY */
}
std::string UsersAgeByFriendsResponse::roundMethod(double var)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(2) << var;
	return stream.str();
}
std::string UsersAgeByFriendsResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();

	int ageSum = 0;
	int usersWithOverOneHoundredFriends = 0;
	for each (User user in users.GetUsers())
	{
		int friendsNumber = user.GetFriendsNumber();
		if (friendsNumber > 100)
		{
			usersWithOverOneHoundredFriends++;
			ageSum += user.GetAge();
		}
	}
	double averageAge = (double)ageSum / usersWithOverOneHoundredFriends;
	std::string result = roundMethod(averageAge);
	this->content.push_back(boost::property_tree::ptree::value_type("File", "UsersAvgAgeFriends.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("UsersAverageFriendsAge", result));
	return this->getContentAsString();
}
