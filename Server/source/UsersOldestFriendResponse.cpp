#include "UsersOldestFriendResponse.h"
#include "Users.h"

UsersOldestFriendResponse::UsersOldestFriendResponse() : Response("UserOldestFriend")
{
	/* EMPTY */
}

std::string UsersOldestFriendResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	/* Read the csv with users */
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();
	int usersNumber = users.GetUsers().size();

	int userID = std::atoi(packet.get<std::string>("User_ID").c_str());
	User user = users.GetUser(userID);

	User oldestFriend;
	for (int userID : user.GetFriends())
	{
		User currentUser = users.GetUser(userID);
		if (currentUser.GetAge() > oldestFriend.GetAge())
		{
			oldestFriend.SetId(currentUser.GetId());
			oldestFriend.SetAge(currentUser.GetAge());
		}
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "userOldestFriends.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("OldestFriendID", std::to_string(oldestFriend.GetId())));
	this->content.push_back(boost::property_tree::ptree::value_type("Age", std::to_string(oldestFriend.GetAge())));

	return this->getContentAsString();
}
