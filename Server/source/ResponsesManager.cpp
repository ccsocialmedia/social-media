#include "ResponsesManager.h"
#include "UserFriendsResponse.h"
#include "GroupByAgeGenderResponse.h"
#include "UsersAgeByFriendsResponse.h"
#include "UsersOldestFriendResponse.h"
#include "GroupByNameResponse.h"
#include "UsersWithSameNameResponse.h"
#include "UsersAverageFriendsResponse.h"

ResponsesManager::ResponsesManager()
{
	this->Responses.emplace("UserFriends", std::make_shared<UserFriendsResponse>());
	this->Responses.emplace("GroupByAgeGender", std::make_shared<GroupByAgeGenderResponse>());
	this->Responses.emplace("UsersAgeByFriends", std::make_shared<UsersAgeByFriendsResponse>());
	this->Responses.emplace("UserOldestFriend", std::make_shared<UsersOldestFriendResponse>());
	this->Responses.emplace("GroupByName", std::make_shared<GroupByNameResponse>());
	this->Responses.emplace("NumberWithSameName", std::make_shared<UsersWithSameNameResponse>());
	this->Responses.emplace("AverageFriends", std::make_shared<UsersAverageFriendsResponse>());
}

std::map<std::string, std::shared_ptr<Framework::Response>> ResponsesManager::getMap() const
{
	return this->Responses;
}
