#include "Users.h"

Users::Users(std::string usersfilePath) :
	csvUtil(usersfilePath)
{ /* EMPTY */}

std::vector<int> Users::MakeFriendships(int userId, unsigned friendsNumber, unsigned usersMaxId)
{
	std::vector<int> randomVector(usersMaxId);
	/* fill the vector with numbers from 0 to usersMaxId - 1 */
	std::generate(randomVector.begin(), randomVector.end(), [n = 0]() mutable { return n++; });

	/* shuffle the vector to make it randomized */
	auto rng = std::default_random_engine{};
	std::shuffle(std::begin(randomVector), std::end(randomVector), rng);

	/* copy first n elements from randomVector to friendships vector and exclude itself */
	std::vector <int> friendships;
	for (unsigned int i = 0; i < friendsNumber; ++i)
	{
		bool itself = randomVector.at(i) == userId;
		if (!itself) // not itself
		{
			friendships.push_back(randomVector.at(i));
		}
		else ++friendsNumber; // skipping itself make the iteration with one step less, so I increase the number of friends by one
	}
	return friendships;
}

void Users::ReadUsers()
{
	/* data looks that: [id, name, age, friendsNumber, gender, list of friends (optional) ] if there are no friends we will set some random friends */

	std::vector<std::vector<std::string>> data = this->csvUtil.GetData();

	for each (const auto& userData in data)
	{
		int id = std::stoi(userData.at(0));
		std::string name = userData.at(1);
		int age = std::stoi(userData.at(2));
		int friendsNumber = std::stoi(userData.at(3));

		Gender gender;
		char cGender = userData.at(4).at(0);
		if (cGender == 'm') //if is male set the gender as a male 
			gender = Gender::Male;
		else // otherwise set as a female
			gender = Gender::Female;

		int nrAttributesBeforeFriends = 5; // exactly: id, name, age, friendsNumber, gender
		int nrFriendsInCsv = userData.size() - nrAttributesBeforeFriends;

		bool invalidNrFriends = friendsNumber != nrFriendsInCsv;
		bool isEmptyFriendsList = true; //userData.at(5) == "";

		User user(id, name, age, friendsNumber, gender);

		if (invalidNrFriends || isEmptyFriendsList)
		{
			user.SetFriends(MakeFriendships(id, friendsNumber, data.size()));
			this->allUsersHaveFriends = false;
		}
		else // has friends
		{
			std::vector<int> friends;
			/* Add the friends from csv */
			for (int i = nrAttributesBeforeFriends; i < userData.size(); ++i)
				friends.push_back(std::stoi(userData.at(i)));
			user.SetFriends(friends);
		}

		/* Add the user in a user list */
		this->users.push_back(user);
	}

}

void Users::OverwriteCsv()
{
	this->csvUtil.WriteData(this->users);
}

User Users::GetUser(int id) const
{
	User user; 
	user.SetId(id);

	return *std::find(this->users.begin(), this->users.end(), user);
}

bool Users::isCsvInconsistent()
{
	return !this->allUsersHaveFriends;
}

std::vector<User> Users::GetUsers() const
{
	return this->users;
}

void Users::SetUsers(std::vector<User> users)
{
	this->users = users;
}

std::string Users::GetUsersFilePath() const
{
	return csvUtil.GetFileName();
}

void Users::SetUsersFilePath(std::string usersFilePath)
{
	csvUtil.SetFileName(usersFilePath);
}
