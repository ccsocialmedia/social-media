#include "UsersWithSameNameResponse.h"
#include "Users.h"

UsersWithSameNameResponse::UsersWithSameNameResponse() : Response("NumberWithSameName")
{
	/* EMPTY */
}

std::string UsersWithSameNameResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	/* Read the csv with users */
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();
	int usersNumber = users.GetUsers().size();

	std::string userName = packet.get<std::string>("Name");
	int nrOfUsers = 0;

	for (User u : users.GetUsers())
	{
		if (userName.compare(u.GetName()) == 0)
		{
			nrOfUsers++;
		}
	}

	this->content.push_back(boost::property_tree::ptree::value_type("File", "userWithSameName.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Number of users", std::to_string(nrOfUsers)));

	return this->getContentAsString();
}