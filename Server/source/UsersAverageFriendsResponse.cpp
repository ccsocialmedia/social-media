#include <iomanip>

#include "Users.h"
#include "UsersAverageFriendsResponse.h"

UsersAverageFriendsResponse::UsersAverageFriendsResponse() : Response("AverageFriends")
{
	/* EMPTY */
}

std::string UsersAverageFriendsResponse::roundMethod(double var)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(2) << var;
	return stream.str();
}

std::string UsersAverageFriendsResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	/* Read the csv with users */
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();
	int usersNumber = users.GetUsers().size();
	int sumAllFriends = 0;

	for (User u : users.GetUsers())
	{
		sumAllFriends += u.GetFriendsNumber();
	}

	int average = sumAllFriends / usersNumber;
	this->content.push_back(boost::property_tree::ptree::value_type("File", "userAverageFriends.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("Average number of friends", std::to_string(average)));
	
	return this->getContentAsString();
}