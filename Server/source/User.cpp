#include "User.h"

User::User(int id, std::string name, int age, int friendsNumber, Gender gender, std::vector<int> friends) :
	id(id),
	age(age),
	friendsNumber(friendsNumber),
	friends(friends),
	name(name),
	gender(gender)
{}

bool User::operator == (const User & other) const
{
	if (this->id == other.id)
		return true;
	return false;
}

bool User::operator!=(const User & other) const
{
	return !(*this == other); // Use exsiting operator == and negate him to avoid duplicate code
}

int User::GetId() const
{
	return this->id;
}

void User::SetId(int id)
{
	this->id = id;
}

int User::GetAge() const
{
	return this->age;
}

void User::SetAge(int age)
{
	this->age = age;
}

int User::GetFriendsNumber() const
{
	return this->friendsNumber;
}

void User::SetFriendsNumber(int friendsNumber)
{
	this->friendsNumber = friendsNumber;
}

std::vector<int> User::GetFriends() const
{
	return this->friends;
}

void User::SetFriends(std::vector<int> friends)
{
	bool isNewFriendsListDiffrentSize = this->friends.size() != friends.size();

	if (isNewFriendsListDiffrentSize)
		this->friends.resize(friends.size()); // Resize the vector for the new list of friends

	/* Modify the friends */
	for (unsigned int frend = 0; frend < friends.size(); ++frend)
		this->friends.at(frend) = friends.at(frend);
}

std::string User::GetName() const
{
	return this->name;
}

void User::SetName(std::string name)
{
	this->name = name;
}

Gender User::GetGender() const
{
	return this->gender;
}

void User::SetGender(Gender gender)
{
	this->gender = gender;
}
