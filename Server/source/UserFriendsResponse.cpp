#include "UserFriendsResponse.h"
#include "Users.h"

UserFriendsResponse::UserFriendsResponse() : Response("UserFriends")
{
	/* EMPTY */
}

std::string UserFriendsResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	/*============================================= Make friends with force ============================================= */
	/* Read the csv with users */
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();

	int usersNumber = users.GetUsers().size();

	this->content.push_back(boost::property_tree::ptree::value_type("File", "usersFriends.txt"));
	this->content.push_back(boost::property_tree::ptree::value_type("UsersNumber", std::to_string(usersNumber)));

	for each (User user in users.GetUsers())
	{
		std::string id = std::to_string(user.GetId());
		std::string name = user.GetName();
		std::string friends;

		//Add friends
		for each (int frn in user.GetFriends())
			friends += std::to_string(frn) + ",";
		
		this->content.push_back(boost::property_tree::ptree::value_type(id, friends));
	}

	return this->getContentAsString();
}
