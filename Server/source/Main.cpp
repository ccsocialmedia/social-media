#include <iostream>
#include <thread>

#include "Users.h"
#include "CsvUtil.h"
#include "Listener.h"

int main()
{
	///*============================================= Make friends with force ============================================= */
	///* Read the csv with users */
	//std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	//Users users(filePath);
	//users.ReadUsers();
	//
	//if (users.isCsvInconsistent())
	//	users.OverwriteCsv();

	/*============================================= Client - Server ============================================= */
	const auto address = boost::asio::ip::make_address("127.0.0.1");
	const unsigned short port = 1313;
	const auto threadsCount = 10;
	/* The io_context is required for all I/O */
	boost::asio::io_context ioContext{ threadsCount };

	/* Create and launch a listening port */
	std::make_shared<Listener>(ioContext, boost::asio::ip::tcp::endpoint{ address, port })->run();

	/* Run the I/O service on the requested number of threads */
	std::vector<std::thread> threads;

	threads.reserve(threadsCount - 1);
	for (auto index = 0; index < threadsCount - 1; ++index)
	{
		threads.emplace_back([&ioContext] { ioContext.run(); });
	}


	ioContext.run();
	
	return EXIT_SUCCESS;
	return 0;
}