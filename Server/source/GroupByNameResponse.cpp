#include "GroupByNameResponse.h"
#include "Users.h"

GroupByNameResponse::GroupByNameResponse() : Response("GroupByName")
{
	/* EMPTY */
}

std::string GroupByNameResponse::interpretPacket(const boost::property_tree::ptree & packet)
{
	/* Read the csv with users */
	std::string filePath("..\\..\\Server\\assets\\social_friends.csv");
	Users users(filePath);
	users.ReadUsers();

	if (users.isCsvInconsistent())
		users.OverwriteCsv();

	std::vector<User> sortedUsers = users.GetUsers();

	// sort using a lambda expression 
	std::sort(sortedUsers.begin(), sortedUsers.end(), [](User a, User b) {
		return a.GetName() < b.GetName();
	});
			
	this->content.push_back(boost::property_tree::ptree::value_type("File", "groupByName.txt"));

	std::string stringUsersName;
	User user = sortedUsers.at(0);
	stringUsersName.append(user.GetName());
	int count = 0;
	for (User ittr : sortedUsers) 
	{
		if (ittr.GetName() != user.GetName())
		{
			this->content.push_back(boost::property_tree::ptree::value_type(user.GetName(), std::to_string(count)));
			stringUsersName.append(" " + ittr.GetName());
			user = ittr;
			count = 0;
		}
		if (ittr == sortedUsers.at(sortedUsers.size()-1)) 
		{
			this->content.push_back(boost::property_tree::ptree::value_type(user.GetName(), std::to_string(count)));
			break;
		}
		++count;
	}
	this->content.push_back(boost::property_tree::ptree::value_type("UsersName", stringUsersName));
	return this->getContentAsString();
}
