#include "CsvUtil.h"

CSVUtil::CSVUtil(std::string filename, std::string delm) :
	fileName(filename), delimeter(delm)
{ /* EMPTY */}

std::vector<std::vector<std::string>> CSVUtil::GetData()
{
	std::ifstream file(this->fileName);

	std::vector<std::vector<std::string> > dataList;

	std::string line = "";

	/* Iterate through each line and split the content using delimeter */
	while (std::getline(file, line))
	{
		std::vector<std::string> vec;
		boost::algorithm::split(vec, line, boost::is_any_of(delimeter));
		dataList.push_back(vec);
	}

	/* Close the File */
	file.close();

	return dataList;
}

void CSVUtil::WriteData(const std::vector<User>& users)
{
	std::ofstream file(this->fileName);

	for each (User user in users)
	{
		file
			<< user.GetId() << ","
			<< user.GetName() << ","
			<< user.GetAge() << ","
			<< user.GetFriendsNumber() << ",";
		
		bool isMale = user.GetGender() == Gender::Male;
		if (isMale)
			file << "m,";
		else file << "f,";

		for (int friendId : user.GetFriends())
		{
			file << friendId << ",";
		}
		file << std::endl;
	}

	file.close();
}

std::string CSVUtil::GetFileName() const
{
	return this->fileName;
}

void CSVUtil::SetFileName(std::string fileName)
{
	this->fileName = fileName;
}

std::string CSVUtil::GetDelimiter() const
{
	return this->delimeter;
}

void CSVUtil::SetDelimiter(std::string delimiter)
{
	this->delimeter = delimeter;
}
