#pragma once

#include<string>
#include<vector>

#include "Gender.h"

/// User class define an user ///
// members <=> id, age, friends, friendsNumber, name, age
class User
{
public:
	/* Constructors */
	User() = default;
	User(int id, std::string name, int age, int friendsNumber, Gender gender, std::vector<int> friends = std::vector<int>());
	
	~User() = default;

	/* Overloaded operators */
public:
	bool operator == (const User& other) const;
	bool operator != (const User& other) const;

	/* Getters & Setters */
public:
	int GetId() const;
	void SetId(int id);

	int GetAge() const;
	void SetAge(int age);

	int GetFriendsNumber() const;
	void SetFriendsNumber(int friendsNumber);

	std::vector<int> GetFriends() const;
	void SetFriends(std::vector<int> friends);

	std::string GetName() const;
	void SetName(std::string name);

	Gender GetGender() const;
	void SetGender(Gender gender);

private:
	int id;
	int age = 0;
	int friendsNumber;
	std::vector<int> friends;
	std::string name;
	Gender gender;
};