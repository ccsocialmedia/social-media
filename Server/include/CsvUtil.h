#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <boost/algorithm/string.hpp>

#include "User.h"
/*
 * A class to read data from a csv file.
 */
class CSVUtil
{
public:
	/* Constructors */
	CSVUtil() = default;
	CSVUtil(std::string filename, std::string delm = ",");

public:
	/* Function to fetch data from a CSV File */
	std::vector<std::vector<std::string> > GetData();

	/* Function to overwrite the csv with a given vector of users*/
	void WriteData(const std::vector<User>& users);

	~CSVUtil() = default;
	

	/* Getters & Setters */
public:
	std::string GetFileName() const;
	void SetFileName(std::string fileName);

	std::string GetDelimiter() const;
	void SetDelimiter(std::string delimiter);

private:
	std::string fileName;
	std::string delimeter;
};