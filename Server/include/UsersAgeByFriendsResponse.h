#pragma once

#include "Response.h"

class UsersAgeByFriendsResponse : public Framework::Response
{
public:
	/* Constructors */
	UsersAgeByFriendsResponse();

public:
	std::string roundMethod(double var);
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};