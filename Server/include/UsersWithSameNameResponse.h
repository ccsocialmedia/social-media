#pragma once

#include "Response.h"

class UsersWithSameNameResponse : public Framework::Response
{
public:
	/* Constructors */
	UsersWithSameNameResponse();

public:
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};