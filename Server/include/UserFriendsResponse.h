#pragma once

#include "Response.h"
#include "Users.h"

class UserFriendsResponse : public Framework::Response
{
public:
	/* Constructors */
	UserFriendsResponse();

public:
	std::vector<User> users;
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};