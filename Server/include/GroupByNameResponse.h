#pragma once

#include "Response.h"

class GroupByNameResponse : public Framework::Response
{
public:
	/* Constructors */
	GroupByNameResponse();

public:
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};