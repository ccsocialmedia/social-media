#pragma once

#include <vector>
#include <random>
#include <iostream>
#include <algorithm>
#include <boost/tokenizer.hpp>

#include "User.h"
#include "CsvUtil.h"

/// Users class is a helper like calss, hold a vector of User class
// members <=> allUsersHaveFriends, users, csvUtil 
class Users
{
public:
	/* Constructors */
	Users() = default;
	Users(std::string usersfilePath);

	~Users() = default;

public:
	/* This function will return a vector of unique friendships (by id) */
	std::vector<int> MakeFriendships(int userId, unsigned friendsNumber, unsigned usersMaxId);
	void ReadUsers();
	void OverwriteCsv();

	/*Getters & Setters */

public:
	/* Returns the searched user on either sorted and unsorted lists */
	User GetUser(int id) const; // get an user by his id

	bool isCsvInconsistent(); // Not every user had friends list seted

	std::vector<User> GetUsers() const;
	void SetUsers(std::vector<User> users);

	std::string GetUsersFilePath() const;
	void SetUsersFilePath(std::string usersFilePath);

private:
	/* If we have read the csv and not all users have friends, this variable will change to true and we will overwrite the csv. */
	bool allUsersHaveFriends; // Defalut is true
	/* All users from the csv file */
	std::vector<User> users; 

private: 
	CSVUtil csvUtil; // Via csvUtil we will access the csv file
};