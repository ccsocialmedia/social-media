/// An enum class that describe the geneder of an user
enum class Gender : char
{
	Male = 'm', //Male
	Female = 'f' //Female
};