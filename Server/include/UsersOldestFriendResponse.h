#pragma once

#include "Response.h"

class UsersOldestFriendResponse : public Framework::Response
{
public:
	/* Constructors */
	UsersOldestFriendResponse();

public:
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};