#pragma once

#include "Response.h"

class UsersAverageFriendsResponse : public Framework::Response
{
public:
	/* Constructors */
	UsersAverageFriendsResponse();

public:
	std::string roundMethod(double var);
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};