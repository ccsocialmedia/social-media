#pragma once

#include "Response.h"

class GroupByAgeGenderResponse : public Framework::Response
{
public:
	/* Constructors */
	GroupByAgeGenderResponse();

public:
	std::string roundMethod(double var);
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};