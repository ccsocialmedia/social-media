﻿# Proiect Social Media #

### Project Number ###
* Numarul proiectului este 10 (Social Media)

### Membrii ###

* Rîtan Mihai-Lucian 		10lf273
* Tutoveanu Andrei Cătălin	10lf273
* Andreea Șerban Nicoleta	10lf273

### Task-uri propuse ###

* Afișare grafic pe procente ale utilizatorilor dupa sex 
* Vârsta medie a utilizatorilor care au mai mult de 100 de prieteni
* Adăugare automată a prietnilor in csv (pe baza id-ului)
* Cel mai vârstnic prieten din lista de prieteni al unui utilizator selectat
* Grupare utilizatori după nume 
* Afisare numar utilizatori cu acelasi nume
* Medie a numarului de prieteni ai utilizatorilor
